import React, { FunctionComponent } from 'react'
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'
import { LoginForm } from './LoginForm'
import { SignUpForm } from './SignUpForm'

export const UserForms: FunctionComponent = () => (
  <section className="hero is-fullheight">
    <div style={{ marginTop: '100px' }}>
      <div className="container">
        <div className="columns">
          <div className="column is-3 is-offset-7">
            <Router>
              <div className="tabs is-toggle is-fullwidth">
                <ul>
                  <li>
                    <Link to="/users/log_in"> Login </Link>
                  </li>
                  <li>
                    <Link to="/users/signUp"> Sign Up</Link>
                  </li>
                </ul>
              </div>
              <Route exact path="/users/log_in" component={LoginForm} />
              <Route path="/users/signUp" component={SignUpForm} />
            </Router>
          </div>
        </div>
      </div>
    </div>
  </section>
)
