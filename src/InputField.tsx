import React, { FunctionComponent } from 'react'
import { Field, FieldProps } from 'formik'
import { LoginFormValues } from './LoginForm'
import { SignUpFormValues } from './SignUpForm'

interface Props {
  readonly title: string
  readonly type: string
}

export type LoginInputProps = FieldProps<LoginFormValues> & Props
export type SignUpInputProps = FieldProps<SignUpFormValues> & Props

interface Error {
  readonly errorMessage: string
}

export const InputField: React.SFC<LoginInputProps | SignUpInputProps> = ({
  title,
  type,
  field,
  form,
}) => {
  return (
    <div className="field">
      <label htmlFor={field.name} className="label">
        <div>{title}</div>
        <Field type={type} className="input" name={field.name} />
        {form.touched[field.name] && form.errors[field.name] ? (
          <div className="error-message">{form.errors[field.name]}</div>
        ) : (
          <div className="error-hide">{'no error'}</div>
        )}
      </label>
    </div>
  )
}
