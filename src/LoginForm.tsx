import React from 'react'
import { Formik, Field } from 'formik'
import { InputField, LoginInputProps } from './InputField'
import * as Yup from 'yup'

const validationSchema = Yup.object().shape({
  userName: Yup.string().required('user name is required'),
  password: Yup.string()
    .min(8, 'Minimum length is 8 characters')
    .required('Enter your password'),
})

export interface LoginFormValues {
  readonly userName: string
  readonly password: string
}

const initialValues: LoginFormValues = {
  userName: '',
  password: '',
}

export const LoginForm: React.SFC = () => {
  return (
    <div className="box">
      <Formik
        initialValues={initialValues}
        onSubmit={values => alert(JSON.stringify(values))}
        validationSchema={validationSchema}
        render={({ handleSubmit }) => (
          <form onSubmit={handleSubmit}>
            <Field
              name="userName"
              render={(innerProps: LoginInputProps) => (
                <InputField {...innerProps} title="Username" type="text" />
              )}
            />
            <Field
              name="password"
              render={(innerProps: LoginInputProps) => (
                <InputField {...innerProps} title="Password" type="password" />
              )}
            />
            <div className="field">
              <button type="submit" className="button is-success">
                Submit
              </button>
            </div>
          </form>
        )}
      />
    </div>
  )
}
