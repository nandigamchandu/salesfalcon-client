import React, { FunctionComponent } from 'react'
import { Formik, Field } from 'formik'
import { InputField, SignUpInputProps } from './InputField'
import * as Yup from 'yup'

export interface SignUpFormValues {
  readonly fullName: string
  readonly userName: string
  readonly email: string
  readonly mobileNumber: string
  readonly dateOfBirth: Date
  readonly designation: string
  readonly password: string
  readonly confirmPassword: string
}

const signUpInitialValues: SignUpFormValues = {
  fullName: '',
  userName: '',
  email: '',
  mobileNumber: '',
  dateOfBirth: new Date('01/01/2019'),
  designation: '',
  password: '',
  confirmPassword: '',
}

const validationSignUpForm = Yup.object().shape({
  fullName: Yup.string().required('full name is required'),
  userName: Yup.string().required('user name is required'),
  mobileNumber: Yup.string().matches(/(\d{3}-\d{3}-\d{4})/, {
    message: 'number format must be 000-000-0000',
    excludeEmptyString: true,
  }),
  designation: Yup.string().required('designation is required'),
  password: Yup.string()
    .min(8, 'Password must contain at least 8 characters')
    .required('Enter your password'),
  confirmPassword: Yup.string()
    .required('Confirm your password')
    .oneOf([Yup.ref('password')], 'Password does not match'),
})

export const SignUpForm: React.SFC = () => {
  return (
    <div className="box">
      <Formik
        initialValues={signUpInitialValues}
        onSubmit={values => alert(JSON.stringify(values))}
        validationSchema={validationSignUpForm}
        render={({ handleSubmit }) => (
          <form onSubmit={handleSubmit}>
            <Field
              name="fullName"
              render={(innerProps: SignUpInputProps) => (
                <InputField {...innerProps} title="Full name" type="text" />
              )}
            />
            <Field
              name="userName"
              render={(innerProps: SignUpInputProps) => (
                <InputField {...innerProps} title="Username" type="text" />
              )}
            />
            <Field
              name="email"
              render={(innerProps: SignUpInputProps) => (
                <InputField {...innerProps} title="Email" type="email" />
              )}
            />
            <Field
              name="mobileNumber"
              render={(innerProps: SignUpInputProps) => (
                <InputField {...innerProps} title="Mobile Number" type="text" />
              )}
            />
            <Field
              name="dateOfBirth"
              render={(innerProps: SignUpInputProps) => (
                <InputField {...innerProps} title="Date Of Birth" type="date" />
              )}
            />
            <Field
              name="designation"
              render={(innerProps: SignUpInputProps) => (
                <InputField {...innerProps} title="Designation" type="text" />
              )}
            />
            <Field
              name="password"
              render={(innerProps: SignUpInputProps) => (
                <InputField {...innerProps} title="Password" type="password" />
              )}
            />
            <Field
              name="confirmPassword"
              render={(innerProps: SignUpInputProps) => (
                <InputField
                  {...innerProps}
                  title="Confirm Password"
                  type="password"
                />
              )}
            />
            <div className="field">
              <button type="submit" className="button is-success">
                Submit
              </button>
            </div>
          </form>
        )}
      />
    </div>
  )
}
