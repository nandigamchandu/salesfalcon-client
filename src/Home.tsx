import React, { FunctionComponent } from 'react'
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'
import { UserForms } from './UserForms'
import { HomePageContent } from './HomePageContent'

export const SalesFalconHome: FunctionComponent = () => (
  <React.Fragment>
    <Router>
      <nav className="navbar is-link has-shadow ">
        <div className="navbar-brand">
          <a className="navbar-item">
            <img src="favicon.ico" />
          </a>
        </div>
        <div className="navbar-menu">
          <div className="navbar-start">
            <a className="navbar-item" href="/">
              Home
            </a>
            <a className="navbar-item">Documentation</a>
          </div>
          <div className="navbar-end">
            <div className="tabs">
              <ul>
                <li>
                  <Link to="/users/signUp"> Sign Up</Link>
                </li>
                <li>
                  <Link to="/users/log_in"> Login </Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
      <Route path="/" exact component={HomePageContent} />
      <Route path="/users/signUp" component={UserForms} />
      <Route path="/users/log_in" component={UserForms} />
    </Router>
    <footer className="footer">
      <div className="content has-text-centered">
        <p>
          <strong>SalesFalcon</strong> by{' '}
          <a href="https://www.technoidentity.com/">technoidentity</a>.
        </p>
      </div>
    </footer>
  </React.Fragment>
)
